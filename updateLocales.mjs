/** This updates locales with what's in database tags so weblate can be aware of them and offer them for translation */

import fs from 'node:fs';
import path from 'node:path';
import langs from '@karaokemugen/langs';
import { unknownLanguages } from './unknownLanguages.mjs';

const allowedTypes = [
	3, // Songtypes
	7, // Misc
	9, // Groups
	10, // Families
	11, // Origins
	12, // Genres
	// 13, // Platforms
	14, // Versions
	15, // Warnings
	16, // Collections
]

const i18n = {
	en: {}
};

const tagDir = 'karaokebase/tags';

const tagFiles = fs.readdirSync(tagDir);

for (const tagFile of tagFiles) {
	const tagData = fs.readFileSync(path.resolve(tagDir, tagFile), 'utf-8');
	const tag = JSON.parse(tagData);
	if (allowedTypes.some(t => tag.tag.types.includes(`${t}`))) {
		const tid = tag.tag.tid;
		if (tag.tag.i18n) {
			for (const language of Object.keys(tag.tag.i18n)) {
				if (unknownLanguages.includes(language)) continue;
				const languages = langs.where('2B', language)
				const lang1 = languages['1'];
				if (!i18n[lang1]) i18n[lang1] = {};
				i18n[lang1][tid] = {};
				i18n[lang1][tid].name = tag.tag.i18n[language];
			}
			if (!Object.keys(tag.tag.i18n).includes('eng')) {
				if (!i18n.en) i18n.en = {};
				if (!i18n.en[tid]) i18n.en[tid] = {};
				i18n['en'][tid].name = tag.tag.name;
			}
		} else {
			i18n['en'][tid] = {};
			i18n['en'][tid].name = tag.tag.name;
		}
		if (tag.tag.description) {
			for (const language of Object.keys(tag.tag.description)) {
				if (unknownLanguages.includes(language)) continue;
				const languages = langs.where('2B', language)
				const lang1 = languages['1'];
				if (!i18n[lang1]) i18n[lang1] = {};
				if (!i18n[lang1][tid]) i18n[lang1][tid] = {};
				i18n[lang1][tid].description = tag.tag.description[language];
			}
		}
	}
}

for (const locale of Object.keys(i18n)) {
	fs.writeFileSync(`locales/${locale}.json`, JSON.stringify(i18n[locale], null, 4), 'utf-8');
}
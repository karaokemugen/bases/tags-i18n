#!/bin/bash
git config user.name "Tags i18n"
git config user.email "server@karaokes.moe"
git --no-pager diff
git add . && git diff --quiet && git diff --cached --quiet
RC=$?
# If no changes are made, return code will be 0.
if [ "$RC" == "0" ]
then
	echo "No changes detected. Aborting gracefully."
	exit 0;
fi
echo "Changes detected. Pushing to repository."
git commit -m "Update tags translations"
git remote set-url origin "https://$GIT_USER:$GIT_PASSWORD@$GIT_URL"
git push

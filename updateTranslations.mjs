/** This updates tags in base with locales when someone uses weblate to translate them  */

import fs from 'node:fs/promises';
import path from 'node:path';
import langs from '@karaokemugen/langs';

const localeDir = 'locales';
const localeFiles = await fs.readdir(path.resolve(localeDir));
const tagDir = 'karaokebase/tags';

const tags = new Map();

const tagFiles = await fs.readdir(path.resolve(tagDir));

for (const tagFile of tagFiles) {
	const tagData = await fs.readFile(path.resolve(tagDir, tagFile));
	const tag = JSON.parse(tagData);
	tags.set(tag.tag.tid, {
		data: tag,
		tagFile
	})
}

for (const localeFile of localeFiles) {
	const localeData = await fs.readFile(path.resolve(localeDir, localeFile), 'utf-8');
	const locale = JSON.parse(localeData);
	const localeName =  path.basename(localeFile, '.json');
	let languages = langs.where('1', localeName.substring(0, 2));
	// Unknown languages. Assume 2B is the full language
	if (!languages) {
		console.log(`Warning: no language found for ${localeName} in langs module`);
		languages = {
			'2B': localeName
		}
	}
	const lang2B = languages['2B'];
	for (const tid of Object.keys(locale)) {
		let isModified = false;
		const tag = tags.get(tid);
		if (!tag) {
			console.log('No tag found for TID: ', tid);
			continue;
		}
		if (locale[tid].name) {
			if (!tag.data.tag.i18n) tag.data.tag.i18n = {};
			if (tag.data.tag.i18n[lang2B] !== locale[tid].name) {
				tag.data.tag.i18n[lang2B] = locale[tid].name;
				isModified = true;
			}
		}
		if (locale[tid].description) {
			if (!tag.data.tag.description) tag.data.tag.description = {};
			if (tag.data.tag.description[lang2B] !== locale[tid].description) {
				tag.data.tag.description[lang2B] = locale[tid].description;
				isModified = true;
			}
		}
		if (isModified) {
			console.log('Updating', tag.tagFile);
			const obj = sortJSON(tag.data.tag);
			tag.data.tag = obj;
			await fs.writeFile(path.resolve(tagDir, tag.tagFile), JSON.stringify(tag.data, null, 2), 'utf-8');
		}
	}
}

function sortJSON(obj) {
	const objOrdered = {};
	Object.keys(obj)
		.sort()
		.forEach(key => {
			objOrdered[key] = obj[key];
		});
	return objOrdered;
}
